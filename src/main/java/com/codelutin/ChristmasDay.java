package com.codelutin;

/**
 * Calculer le jour de la semaine où tombe Noël pour une année, entre 2000 et 2099,
 * saisie au clavier, sachant que :
 * En 2000, Noël était un lundi
 * D’une année sur l’autre, hors année bissextile, Noël progresse d’un jour
 * Pour les années bissextiles, la progression est de deux jours
 * 2000 était bissextile, ainsi que 2004 …
 */

import java.util.Arrays;
import java.util.Scanner;

public class ChristmasDay {

    public static void main(String[] args) {

        Scanner keyboardInput = new Scanner(System.in);

        // On crée un tableau weekDays pour stocker les jours de la semaine
        String[] weekDays= {"Lundi","Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"};

        // On saisit une année et on lui retire 2000 > (year) correspond au nombre de jours à ajouter
        int year = keyboardInput.nextInt() - 2000;

        // Pour les bissextiles, au lieu de progresser d'un jour, on progresse de 2 > Ce qu'on veut obtenir, c'est le
        // nombre d'années bissextiles qui se sont écoulées entre 2000 et l'année saisie > Pour cela, on a juste à
        // diviser year par 4 > On obtient les jours supplémentaires des années bissextiles (yearB) à ajouter au calcul
        // final
        int yearB = (year/4);

        // On additionne le nombre de jours (year) aux jours supplémentaires des années bissextiles (yearB) > On fait
        // ensuite un modulo de 7 (pour le nombre de fois où 7 jours se sont écoulés entre 2000 et l'année saisie) et
        // obtenir le jour correspondant (christmasDay)
        int christmasDay = (year + yearB)%7;

        // On affiche le jour correspondant dans le tableau weekDays
        String day = weekDays[christmasDay];
        System.out.println("Noël de l'année 20" + year + " tombe un " + day);
    }

}
