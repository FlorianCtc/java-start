package com.codelutin;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Hello world!
 */
public class Exo {

    public static void main(String[] args) {
        //System.out.println("Hello World!");
        int saisie1, saisie2, resultat;
        Scanner keyboardInput = new Scanner(System.in);
        System.out.println("Saisissez un premier entier");
        saisie1 = keyboardInput.nextInt();
        System.out.println("Saisissez un deuxième entier");
        saisie2 = keyboardInput.nextInt();
        if (saisie1 > saisie2){
            resultat = saisie1;
        } else {
            resultat = saisie2;
        }
        System.out.println("L'entier le plus grand est " + resultat);

    }
}
